#!/bin/bash

function game_help() {
	clear
	printf "<Enter> - make a turn\n"
	printf "<S> - game stat\n"
	printf "<Esc> - exit\n"
}

function game_stat() {
	clear
	echo "Current game stats:"
	echo "$PLAYER - $PLSTAT ; $HOSTNAME - $MCHSTAT"
}

# initializations

clear
printf "Enter your name: "
read PLAYER
clear

PLAYER="${PLAYER:-$USER}"
echo "Hi, $PLAYER"

ROUND="${ROUND:-0}"
PLSTAT=0
MCHSTAT=0

# main game cycle
while true
do
	printf "Round $ROUND:\n" 
	printf "<H> - display help\n"

	# read user command
	read -n 1 -s -r INPUT
	IORD=$(printf "%d\n" \'$INPUT)
	if [ "$INPUT" = "h" ] || [ "$INPUT" = "H" ]; then
		game_help
	elif [ "$INPUT" = "S" ] || [ "$INPUT" = "s" ]; then
		game_stat
	elif [ $IORD -eq 0 ]; then
		select t in stone scissors paper
		do
			case $t in
				"stone")
					clear
					echo "$PLAYER choose stone"
					rnd=$(shuf -i 1-3 -n 1)
					case $rnd in
						1)
							echo "$HOSTNAME choose stone"
							echo "DRAW"
							;;
						2)
							echo "$HOSTNAME choose scissors"
							echo "$PLAYER wins"
							PLSTAT=$((PLSTAT+1))
							;;
						3)
							echo "$HOSTNAME choose paper"
							echo "$HOSTNAME wins"
							MCHSTAT=$((MCHSTAT+1))
							;;
					esac
					ROUND=$((ROUND+1))
					break
					;;
				"scissors")
					clear
					echo "$PLAYER choose scissors"
					rnd=$(shuf -i 1-3 -n 1)
					case $rnd in
						1)
							echo "$HOSTNAME choose stone"
							echo "$HOSTNAME wins"
							MCHSTAT=$((MCHSTAT+1))
							;;
						2)
							echo "$HOSTNAME choose scissors"
							echo "DRAW"
							;;
						3)
							echo "$HOSTNAME choose paper"
							echo "$PLAYER wins"
							PLSTAT=$((PLSTAT+1))
							;;
					esac
					ROUND=$((ROUND+1))
					break;
					;;
				"paper")
					clear
					echo "$PLAYER choose paper"
					rnd=$(shuf -i 1-3 -n 1)
					case $rnd in
						1)
							echo "$HOSTNAME choose stone"
							echo "$PLAYER wins"
							PLSTAT=$((PLSTAT+1))
							;;
						2)
							echo "$HOSTNAME choose scissors"
							echo "$HOSTNAME wins"
							MCHSTAT=$((MCHSTAT+1))
							;;
						3)
							echo "$HOSTNAME choose paper"
							echo "DRAW"
							;;
					esac
					ROUND=$((ROUND+1))
					break;
					;;
				*) echo "Invelid option";;
			esac
		done
	elif [ $IORD -eq 27 ]; then
		exit
	fi

done





 
